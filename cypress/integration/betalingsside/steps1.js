import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//Registrert kjøpet vellykket
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {

    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#list>li').eq(0).should('contain.text', 'Stratos');
    cy.get('#list>li').eq(1).should('contain.text', 'Hubba bubba');
    cy.get('#list>li').eq(2).should('contain.text', 'Smørbukk');
    cy.get('#list>li').eq(3).should('contain.text', 'Hobby');

    cy.get('#price').should('have.text', '33');


});

And(/^trykket på Gå til betaling$/, () => {

    cy.get('#goToPayment').click();

});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {

    cy.visit("/payment.html");
    cy.get('#fullName').type('Lise Spogun');
    cy.get('#address').type('Prestekragevegen 9');
    cy.get('#postCode').type('7050');
    cy.get('#city').type("Trondheim");
    cy.get('#creditCardNo').type("1234567890123456");


});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('input[type=submit]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('exist');
})


//Få feilmeldinger
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {

    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#list>li').eq(0).should('contain.text', 'Stratos');
    cy.get('#list>li').eq(1).should('contain.text', 'Hubba bubba');
    cy.get('#list>li').eq(2).should('contain.text', 'Smørbukk');
    cy.get('#list>li').eq(3).should('contain.text', 'Hobby');

    cy.get('#price').should('have.text', '33');


});

And(/^trykket på Gå til betaling$/, () => {

    cy.get('#goToPayment').click();

});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').type('Lise Spogun');
    cy.get('#address').type('Prestekragevegen 9');
    cy.get('#postCode').type('7050');
    cy.get('#city').type("Trondheim");
    cy.get('#creditCardNo').type("1234567890123")
    cy.get('input[type=submit]').click();

});

Then(/^skal jeg få feilmeldinger for disse$/, () => {

    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer')


})
