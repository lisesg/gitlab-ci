import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {


    cy.get('#list>li').eq(0).should('contain.text', 'Stratos');
    cy.get('#list>li').eq(1).should('contain.text', 'Hubba bubba');
    cy.get('#list>li').eq(2).should('contain.text', 'Smørbukk');
    cy.get('#list>li').eq(3).should('contain.text', 'Hobby');





});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});


// Scenario: Slette varer i handlekurven
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#list').eq(0).should('contain.text', 'Hubba bubba');
    cy.get('#price').should('have.text', '8')




});

When(/^jeg sletter varer$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#deleteItem').click();

});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#list').should('not.contain', 'Hubba bubba')

});

// Scenario: Oppdater varer i handlekurven

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#list').eq(0).should('contain.text', 'Hubba bubba');
    cy.get('#price').should('have.text', '8')




});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('3');
    cy.get('#saveItem').click();



})

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#list').eq(0).should('contain.text', '3 Hubba bubba');


})
